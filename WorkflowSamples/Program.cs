﻿using System;
using System.Collections.Generic;
using System.Linq;
using Marti.Workflow.Soa.Interfaces;
using Marti.Workflow.Soa.Interfaces.Dom.Activities;
using Marti.Workflow.Soa.Services.Repository;
using Marti.Workflow.Utility;

namespace WorkflowSamples
{
    class Program
    {
        private static readonly Dictionary<ActivityType, Action<IPdlProcess, ActivityNodeBase>> _map =
            new Dictionary<ActivityType, Action<IPdlProcess, ActivityNodeBase>>
            {
                {ActivityType.InvokeProcess, PrintInvokeProcess},
                {ActivityType.Invoke, PrintInvoke}
            };

        static void Main(string[] args)
        {
            var repo = new FileSystemSoaRepository();

            Console.WriteLine("Loading repo");
            repo.LoadRepository(@"c:\FORIS\MAIN\Export\SOA");

            var processes = repo.GetPdlProcesses();
            foreach (var process in processes)
            {
                Console.WriteLine("=== Process {0} ===", process.Name);
                
                Console.WriteLine("Input variables:");
                foreach (var variable in process.Interface.Set)
                {
                    if (!string.IsNullOrEmpty(variable.Variable))
                    {
                        Console.WriteLine("[V] {0}: {1}", variable.Variable, GetVariableContract(process, variable.Variable));
                    }
                    else
                    {
                        Console.WriteLine("[LC] {0}", variable.LargeCollection);
                    }
                }

                Console.WriteLine("Useful stuff:");
                var queue = new Queue<ActivityNodeBase>();
                if (process.Instantiation != null)
                    queue.Enqueue(process.Instantiation);

                if (process.Execution != null)
                    queue.Enqueue(process.Execution);

                while (queue.Count > 0)
                {
                    var node = queue.Dequeue();
                    if (!node.Children.IsNullOrEmpty())
                    {
                        foreach (var child in node.Children.OfType<ActivityNodeBase>())
                        {
                            queue.Enqueue(child);
                        }
                    }

                    if (_map.ContainsKey(node.Type))
                    {
                        _map[node.Type](process, node);
                    }
                }
            }

            Console.ReadKey();
        }

        private static void PrintInvoke(IPdlProcess process, ActivityNodeBase node)
        {
            var invokeNode = (InvokeNode)node;
            var serviceDecl = process.Declarations.Services.FirstOrDefault(x => x.Name == invokeNode.Service);
            if (serviceDecl == null)
            {
                throw new InvalidOperationException("Service decl not found!");
            }

            Console.WriteLine("[Invoke] {0}.{1} ({2})", serviceDecl.Contract, invokeNode.Operation, invokeNode.Name);
        }

        private static void PrintInvokeProcess(IPdlProcess process, ActivityNodeBase node)
        {
            Console.WriteLine("[InvokeProcess] {0}", ((InvokeProcessNode)node).ProcessTypeCode);
        }

        private static string GetVariableContract(IPdlProcess process, string varName)
        {
            var decl = process.Declarations.Variables.FirstOrDefault(x => x.Name == varName);
            if (decl == null)
            {
                throw new InvalidOperationException();
            }

            var type = process.Declarations.Types.FirstOrDefault(x => x.Name == decl.Type);
            if (type == null)
            {
                throw new InvalidOperationException();
            }

            if (string.IsNullOrEmpty(type.Contract))
            {
                // TODO: This is a complex type. Use type.ComplexMembers
                return "[Complex type]";
            }

            return type.Contract;
        }
    }
}
